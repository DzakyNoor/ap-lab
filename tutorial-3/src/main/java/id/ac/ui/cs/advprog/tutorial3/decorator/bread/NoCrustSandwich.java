package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class NoCrustSandwich extends Food {
    public NoCrustSandwich() {
        //TODO Implement
        this.description = "No Crust Sandwich";
        this.cost = 2.00;
    }

    /*
    @Override
    public getDescription(){
    	return "No Crust Sandwich";
    }*/

    @Override
    public double cost() {
        //TODO Implement
        return this.cost;
    }
}
