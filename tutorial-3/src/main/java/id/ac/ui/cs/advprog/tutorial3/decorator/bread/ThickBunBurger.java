package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThickBunBurger extends Food {
    public ThickBunBurger() {
        //TODO Implement
        this.description = "Thick Bun Burger";
        this.cost = 2.50;
    }

    @Override
    public double cost() {
        //TODO Implement
        return this.cost;
    }
}
