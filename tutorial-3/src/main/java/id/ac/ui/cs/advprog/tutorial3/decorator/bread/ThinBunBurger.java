package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThinBunBurger extends Food {
    public ThinBunBurger() {
        //TODO Implement
        this.description = "Thin Bun Burger";
        this.cost = 1.50;
    }

    @Override
    public double cost() {
        //TODO Implement
        return this.cost;
    }
}
