package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class CrustySandwich extends Food {
    public CrustySandwich() {
        //TODO Implement
        this.description = "Crusty Sandwich";
        this.cost = 1.00;
    }

    @Override
    public double cost() {
        //TODO Implement
        return this.cost;
    }
}
