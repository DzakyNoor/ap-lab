package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class Main {
    public static void main(String[] args) {
        //

        BreadProducer test1 = BreadProducer.CRUSTY_SANDWICH;
        Food testFood1 = test1.createBreadToBeFilled();
        System.out.println(testFood1.getDescription());

        FillingDecorator test2 = FillingDecorator.BEEF_MEAT;
        testFood1 = test2.addFillingToBread(testFood1);
        System.out.println(testFood1.getDescription());
        System.out.println(testFood1.cost());
        
        testFood1 = test2.addFillingToBread(testFood1);
        System.out.println(testFood1.getDescription());
        System.out.println(testFood1.cost());


        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        //assertEquals(2.50, thickBunBurgerSpecial.cost(), 0.001);
        System.out.println(thickBunBurgerSpecial.getDescription() + " costs " + thickBunBurgerSpecial.cost());


        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(
                thickBunBurgerSpecial);
        //assertEquals(8.50, thickBunBurgerSpecial.cost(), 0.001);
        System.out.println(thickBunBurgerSpecial.getDescription() + " costs " + thickBunBurgerSpecial.cost());


        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(
                thickBunBurgerSpecial);
        //assertEquals(10.50, thickBunBurgerSpecial.cost(), 0.001);
        System.out.println(thickBunBurgerSpecial.getDescription() + " costs " + thickBunBurgerSpecial.cost());


        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(
                thickBunBurgerSpecial);
        //assertEquals(10.90, thickBunBurgerSpecial.cost(), 0.001);
        System.out.println(thickBunBurgerSpecial.getDescription() + " costs " + thickBunBurgerSpecial.cost());


        thickBunBurgerSpecial = FillingDecorator.LETTUCE.addFillingToBread(
                thickBunBurgerSpecial);
        //assertEquals(11.65, thickBunBurgerSpecial.cost(), 0.001);
        System.out.println(thickBunBurgerSpecial.getDescription() + " costs " + thickBunBurgerSpecial.cost());


        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurgerSpecial);
        //assertEquals(11.95, thickBunBurgerSpecial.cost(), 0.001);

        System.out.println(thickBunBurgerSpecial.getDescription() + " costs " + thickBunBurgerSpecial.cost());


        //Crusty Sandiwich with Beef Meat, Chicken Meat, Using Tomato and Chili Sauce
        Food doubleBeefChickenDoubleSauceSandwich =
                BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        //assertEquals(1.00, doubleBeefChickenDoubleSauceSandwich.cost(), 0.001);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription() + " costs " + doubleBeefChickenDoubleSauceSandwich.cost());


        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.BEEF_MEAT.addFillingToBread(
                doubleBeefChickenDoubleSauceSandwich);
        //assertEquals(7.00, doubleBeefChickenDoubleSauceSandwich.cost(), 0.001);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription() + " costs " + doubleBeefChickenDoubleSauceSandwich.cost());


        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                doubleBeefChickenDoubleSauceSandwich);
        //assertEquals(11.50, doubleBeefChickenDoubleSauceSandwich.cost(), 0.001);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription() + " costs " + doubleBeefChickenDoubleSauceSandwich.cost());


        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHILI_SAUCE.addFillingToBread(
                doubleBeefChickenDoubleSauceSandwich);
        //assertEquals(11.80, doubleBeefChickenDoubleSauceSandwich.cost(), 0.001);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription() + " costs " + doubleBeefChickenDoubleSauceSandwich.cost());


        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        //assertEquals(12.00, doubleBeefChickenDoubleSauceSandwich.cost(), 0.001);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription() + " costs " + doubleBeefChickenDoubleSauceSandwich.cost());

    }
}