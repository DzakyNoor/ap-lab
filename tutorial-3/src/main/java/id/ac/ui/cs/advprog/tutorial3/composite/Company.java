package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        //TODO Implement
        this.employeesList.add(employees);
    }

    public double getNetSalaries() {
        //TODO Implement
        //List<Employees> emp = getAllEmployees();
        double netSalaries = 0;
        for(int i = 0; i < this.employeesList.size(); i++){
            netSalaries += this.employeesList.get(i).getSalary();
        }
        return netSalaries;
    }

    public List<Employees> getAllEmployees() {
        //TODO Implement
        return this.employeesList;
    }
}
