package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //

        Company company = new Company();

        Ceo luffy = new Ceo("Luffy", 500000.00);
        company.addEmployee(luffy);

        Cto zorro = new Cto("Zorro", 320000.00);
        company.addEmployee(zorro);

        BackendProgrammer franky = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(franky);

        BackendProgrammer usopp = new BackendProgrammer("Usopp", 200000.00);
        company.addEmployee(usopp);

        FrontendProgrammer nami = new FrontendProgrammer("Nami",66000.00);
        company.addEmployee(nami);

        FrontendProgrammer robin = new FrontendProgrammer("Robin", 130000.00);
        company.addEmployee(robin);

        UiUxDesigner sanji = new UiUxDesigner("sanji", 177000.00);
        company.addEmployee(sanji);

        NetworkExpert brook = new NetworkExpert("Brook", 83000.00);
        company.addEmployee(brook);

        //NEED TO UPDATE IF YOU WANT TO WORK ADDITIONAL PROBLEM SET
        //TODO Implement
        SecurityExpert chopper = new SecurityExpert("Chopper", 70000.00);
        company.addEmployee(chopper);


        List<Employees> tempList = company.getAllEmployees();

        for(int i = 0; i < tempList.size(); i++){
            Employees tempEmp = tempList.get(i);
            System.out.println(tempEmp.getName() + " as " + tempEmp.getRole() + "; Salary : " + tempEmp.getSalary());
        }

        System.out.println("\nCompany net salary : " + company.getNetSalaries());
    }
}